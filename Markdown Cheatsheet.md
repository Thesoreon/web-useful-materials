![Image of Me](https://assets.gitlab-static.net/uploads/-/system/user/avatar/2441280/avatar.png)

## HEADER

<b>Syntax:</b> # Text
> Number of # changes the size of header (h1 - h6, # - ######)

## ITALIC

<b>Syntax: </b> * Text *
> <b>NOTE</b>: Between * and <b>Text</b> there is no space

## QUOTE

<b>Syntax:</b> > Text

## LIST

<b>Syntax:</b> * Text OR for sublist make TWO spaces before the *

## IMAGE

<b>Syntax:</b> ![desc(Optional)] (url)
> <b>NOTE</b>: Between ] and ( there is no space