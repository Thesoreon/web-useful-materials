# FONT AWESOME [link](https://fontawesome.com/)

Set of useful icons for websites.

## USAGE

The biggest cons of this icon toolkit is that it can be implemented in most modern frameworks and libraries such as:

* [React](https://fontawesome.com/how-to-use/on-the-web/using-with/react)
* [Angular](https://fontawesome.com/how-to-use/on-the-web/using-with/angular)
* [Vue.js](https://fontawesome.com/how-to-use/on-the-web/using-with/vuejs)
* [Ember](https://fontawesome.com/how-to-use/on-the-web/using-with/ember)

AND CSS PREPROCESSORS LIKE:

* [SASS](https://fontawesome.com/how-to-use/on-the-web/using-with/sass)
* [LESS](https://fontawesome.com/how-to-use/on-the-web/using-with/less)

> <b>NOTE:</b> links will redirect you to official page of FontAwesome.

## LICENSE

* Icons are under <b>CC BY 4.0 License</b>
* Fonts are under <b>SIL OFL 1.1 License</b>
* Source code is under <b>MIT License</b>
