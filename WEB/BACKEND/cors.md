# CORS (<b>`C`</b>ross-<b>`o`</b>rigin <b>`r`</b>esource <b>`s`</b>haring)

### DEFINITION

CORS is a mechanism that allows getting resources from another domain.

> EXAMPLE: You are at http://localhost:8080 and you are sending `http post` request to http://localhost:3000, but probably it has failed, why?

### It does not apply for:
- Images
- Scripts
- Stylesheets
- Iframes & Videos

### Request that are forbidden by [same-origin security policy!](https://en.wikipedia.org/wiki/Same-origin_policy)
- Ajax requests

## How CORS works

![FlowChart](https://upload.wikimedia.org/wikipedia/commons/c/ca/Flowchart_showing_Simple_and_Preflight_XHR.svg)
> Source: https://en.wikipedia.org/wiki/File:Flowchart_showing_Simple_and_Preflight_XHR.svg

If request fall in added latency part, the curent steps happens.
1. Browser sent `options` request to server with new HTTP header `Origin`, value of this header is <b>domain</b> where was the request sent. 
2. Server responds with the HTTP header `Access-Control-Allow-Origin` that indicicates which `Origin` values are allowed.

> NOTE: `Access-Control-Allow-Origin: *` Header with * wildcard allows all domains.