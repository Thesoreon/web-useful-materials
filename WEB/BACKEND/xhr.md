# XHR (<b>`X`</b>ML<b>`H`</b>ttp<b>`R`</b>equest)

### Definition 

XHR is an <b>API</b>, whích is defined as an object and its [methods](https://gitlab.com/Thesoreon/web-useful-materials/blob/master/WEB/BACKEND/crud.md) can transfer data between <b>web browser</b> (client) and <b>web server</b>.

The respond from the server can be in many types such as: 
- HTML
- Plain text
- JSON, <b>but JSONP does not use XHR!</b>
- XML

> Also note, that XHR is fundamental for modifying already loaded website aka concept of AJAX.

### Protocols

As the name already showed, we can use XHR with the <b>http(s)</b> protocols, but it also supports <b>file or ftp protocols.</b>

### Useful resources

- https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
- https://en.wikipedia.org/wiki/XMLHttpRequest
- https://www.w3schools.com/xml/xml_http.asp