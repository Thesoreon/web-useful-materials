# CRUD

### Definition

CRUD stands for 4 basic operation of persistent storage.
> Persistent storage could be SQL database, but globally it means that your data are saved on some kind of device, that doesn't erase data on shutdown.
(for example hard drives)

- <b>`C`reate</b>
> In SQL database this stands for query string with `INSERT` statement

- <b>`R`ead</b>
> In SQL database this stands for query string with `SELECT` statement

- <b>`U`pdate</b>
> In SQL database this stands for query string with `UPDATE` statement

- <b>`D`elete</b>
> IN SQL database this stands for query string with `DELETE` statement

### In web development

Basic example where CRUD is implemented is simple blog.

- User adds new post (<b>`C`REATE</b>)

- User is reading latest post (<b>`R`EAD</b>)

- User made mistake in post, so he edited it (<b>`U`PDATE</b>)

- User needs for some reason delete latest post (<b>`D`ELETE</b>)

### When building RESTful apps (aka making request with http for CRUD operations)

In HTTP requests are these four operation defined like this:

- <b>post</b> stands for `C`reate
- <b>get</b> stands for `r`ead
- <b>put</b> stands for `u`pdate
- <b>`d`elete</b> is same.