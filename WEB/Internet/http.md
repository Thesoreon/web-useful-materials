# HTTP (<b>`H`</b>yper<b>`t`</b>ext <b>`T`</b>ransfer <b>`P`</b>rotocol)

is <b>application protocol</b>. It is the foundation of communication in World Wide Web.
Basically, one hypertext document (for example HTML file) contains link (anchor tag) to another hypertext document. That's the basic of the communication in World Wide Web.

### HTTP METHODS

HTTP provides a list of methods (also known as "verbs"), which specifies type of request that is made for server. 

> More about these (POST, GET, PUT, DELETE) methods can be found [there](https://gitlab.com/Thesoreon/web-useful-materials/blob/master/WEB/BACKEND/crud.md)!

- GET
- POST
- PUT
- OPTIONS
- DELETE
- HEAD

> There are many more methods (connect, trace, patch..)! 
> More you can learn on this <b>[site](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)</b>.

### SOURCES
(Very recommend to read these)

- [Wikipedia](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol)
- [Blog Post at Medium.com](https://medium.freecodecamp.org/restful-services-part-i-http-in-a-nutshell-aab3bfedd131)
