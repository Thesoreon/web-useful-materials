# List of todos for this repo.

## GENERAL

* Make markdown file where are most common license explained <b>(LICENSES.md)</b>
* Make markdown file with list of useful Youtube videos (and other materials) <b>(useful.md)</b>

# Mathematics

* Make markdown file about vectors <b>(vectors.md)</b>

## INTERNET

* Make markdown file where is HTTP Protocol explained <b>(http.md)</b>
* Make markdown file where are HTTP requests, headers,.. explained. <b>(http_full.md)</b>
* Make md file about URL & URI <b>(url.md)</b>

## DESKTOP APPS

* Make markdown file about electron <b>(electron.md)</b>

## FRONTEND



## BACKEND

* Make markdown file where are relational databases explained <b>(relational_databases.md)</b>
* Make markdown file about nginx <b>(nginx.md)</b>
* Make markdown file about ORM <b>(orm.md)</b>

## OTHER

* Make markdown file about active directory <b>(active_directory.md)</b>

## COMPLETED TODOS

- [x] created this repo
- [x] Make markdown file where is concept of CORS explained <b>(cors.md)</b>
- [x] Make markdown file about CRUD <b>(crud.md)</b>
- [x] Make markdown file where is XHR explained <b>(XHR.md)</b>